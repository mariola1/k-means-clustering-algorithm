import csv
import sys
from src.HistoryPrinter import HistoryPrinter
from src.PlotPrinter import PlotPrinter


def save_csv_as_list(csv_file):
    with open(csv_file, 'r') as f:
        reader = csv.reader(f)
        data = list(reader)
    return data


"""Converts string 2d tuples into the numerical 2d points."""
def return_list_of_points(list_of_data):
    points = []
    [points.append((float(x), float(y))) for (x, y) in list_of_data]
    return points


def k_means_clustering_algorithm(csv_file, k):
    csv_file_as_list = save_csv_as_list(csv_file)
    list_of_points = return_list_of_points(csv_file_as_list)
    history = HistoryPrinter.cluster_with_history(list_of_points, k)
    print("The total number of steps: {number_of_steps}".format(number_of_steps=len(history)))
    print("The history of algorithm:\n")
    HistoryPrinter.print_history(history)
    (points_groups, centroids) = history[len(history) - 1]
    """display the situation graphically at the last step"""
    PlotPrinter.draw_plot(points_groups, centroids)


def run_program():
    if len(sys.argv) < 3:
        sys.exit('Please, input as arguments:\n' +
                 '1. the name of the CSV file with the 2d coordinates of the' +
                 ' points to be clustered.\n' +
                 '2. the number of the clusters to be created from the points.\n')

    csv_file = sys.argv[1]
    k = int(sys.argv[2])
    k_means_clustering_algorithm(csv_file, k)


if __name__ == "__main__":
    """Program start"""""
    run_program()
