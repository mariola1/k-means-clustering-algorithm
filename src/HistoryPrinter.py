from src.KMeansClusteringAlgorithm import KMeansClusteringAlgorithm


class HistoryPrinter:

    @staticmethod
    def cluster_with_history(list_of_points, k):
        centroids = KMeansClusteringAlgorithm.choose_init_centroids(list_of_points, k)
        history = []
        while True:
            point_groups = KMeansClusteringAlgorithm.assign_point_to_cluster(centroids, list_of_points)
            history.append((point_groups, centroids))
            new_centroids = KMeansClusteringAlgorithm.choose_centroids(point_groups, k)
            for i in range(0, k):
                if new_centroids[i] != centroids[i]:
                    return history
            centroids = new_centroids

    """function prints all clusters and all points that belong to particular cluster """

    @staticmethod
    def print_points_in_cluster(point_groups):
        max_group = 0
        all_groups = {}
        for index, (point, group) in enumerate(point_groups):
            if max_group < group:
                max_group = group
            if all_groups.get(group, None) is None:
                all_groups[group] = []
            all_groups[group].append((index, point))
        for index, element in enumerate(all_groups):
            print("Cluster {number}: {list_of_points}".format(number=index, list_of_points=all_groups[index]))

    @staticmethod
    def print_history(history):

        for index, (point_groups, centroids) in enumerate(history):
            print("Step {step}:".format(step=index))
            HistoryPrinter.print_points_in_cluster(point_groups)
            print("Centroids {centroids}: \n".format(centroids=centroids))
