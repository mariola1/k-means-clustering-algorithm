import math


class KMeansClusteringAlgorithm:

    @staticmethod
    def __count_euclidean_distance(x1_y1, x2_y2):
        x1, y1 = x1_y1
        x2, y2 = x2_y2
        return math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))

    """Returns the distance of a point from the closest point in list_of_centroids.
    The distance between point X and set of points {p1,...,pn} is the distance between point X and the nearest point
    from set {p1,..pn} other words d(X,{p1,...,pn})=min d(X,pi, i=1,...,n)"""

    @staticmethod
    def __find_min_distance(list_of_centroids, candidate):
        min_dist = KMeansClusteringAlgorithm.__count_euclidean_distance(list_of_centroids[0], candidate)
        for centroid in list_of_centroids:
            dist = KMeansClusteringAlgorithm.__count_euclidean_distance(centroid, candidate)
            if min_dist > dist:
                min_dist = dist
        return min_dist

    """For each point function returns the number of group which is in the shortest distance from given point"""

    @staticmethod
    def __find_closest_group(point, list_of_centroids):
        min_dist = KMeansClusteringAlgorithm.__count_euclidean_distance(point, list_of_centroids[0])
        selected_group = 0
        for i in range(1, len(list_of_centroids)):
            dist = KMeansClusteringAlgorithm.__count_euclidean_distance(point, list_of_centroids[i])
            if min_dist > dist:
                min_dist = dist
                selected_group = i
        return selected_group

    """Returns k initial centroids for given list of points"""

    @staticmethod
    def choose_init_centroids(list_of_points, k):
        centroids = [list_of_points[0]]
        while len(centroids) < k:
            candidate = list_of_points[0]
            dist = KMeansClusteringAlgorithm.__find_min_distance(centroids, candidate)
            for point in list_of_points:
                candidate_dist = KMeansClusteringAlgorithm.__find_min_distance(centroids, point)
                if dist < candidate_dist:
                    dist = candidate_dist
                    candidate = point
            centroids.append(candidate)
        return centroids

    """assignment points to the clusters"""

    @staticmethod
    def assign_point_to_cluster(list_of_centroids, list_of_points):
        new_point_groups = []
        for point in list_of_points:
            new_point_groups.append((point, KMeansClusteringAlgorithm.__find_closest_group(point, list_of_centroids)))
        return new_point_groups

    """functions sets down new centroids based on clusters' groups"""

    @staticmethod
    def choose_centroids(point_groups, k):
        centroid_xs = [0] * k
        centroid_ys = [0] * k
        group_s = [0] * k
        centroids = []
        for ((x, y), group) in point_groups:
            centroid_xs[group] += x
            centroid_ys[group] += y
            group_s[group] += 1
        for group in range(0, k):
            centroids.append((float((centroid_xs[group]) / group_s[group]), float(centroid_ys[group] / group_s[group])))
        return centroids
