import matplotlib.pyplot as plt

"""Draws the points and centroids. If they belong to the same group,
then it colors them with the same color. Points are drawn as filled
stars. Centroids are drawn as filled deltoids."""
class PlotPrinter:

    @staticmethod
    def draw_plot(points_groups, centroids):
        x_s = []
        y_s = []
        colors = []
        for ((x, y), group) in points_groups:
            x_s.append(x)
            y_s.append(y)
            colors.append(group)
        centroid_xs = []
        centroid_ys = []
        centroid_color = []
        for index, (x, y) in enumerate(centroids):
            centroid_xs.append(x)
            centroid_ys.append(y)
            centroid_color.append(index)
        plt.scatter(x_s, y_s, c=colors, s=[50] * len(x_s), marker='*')
        plt.scatter(centroid_xs, centroid_ys, c=centroid_color, s=[120] * len(centroid_xs), marker='d')
        plt.show()
