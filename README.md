# k-means-clustering-algorithm
Project contains implemented k-means-clustering-algorithm. Each step of choice centroids, indicating clusters and
assigning points to particular cluster are described. Final step is visualized by the use of  matplotlib library.

To run this program there is need to provide:

    1. the name of the CSV file with the 2d coordinates of the points to be clustered
    2. the number of the clusters to be created from the points

